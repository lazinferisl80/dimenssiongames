import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  logo: string;
  logoAlt: string;
  menu: string;
  novedades: string;
  constructor() {
    this.logo = "https://www.gmkfreelogos.com/logos/G/img/Games.gif";
    this.logoAlt = "mando playstation";
    this.menu = "";
    this.novedades = "";
  }

  ngOnInit(): void {}
}
