import { Component, OnInit } from "@angular/core";
import { Footer } from "./Interface-Footer";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit {
  footer: any | Footer = {};
  agradecimientos: string[];
  contactos: string[];
  recuerda: string[];

  constructor() {
    this.agradecimientos = ["agradecimientos a nuestros seguidores"];
    this.contactos = ["contacta con nosotros"];
    this.recuerda = ["no nos olvides"];
  }

  ngOnInit(): void {}
}
