export interface Footer {
  agradecimientos: string;
  contactos: string;
  recuerda: string;
}
