import { Component, OnInit, Input } from "@angular/core";
import { InterfaceBody } from "../Interface-body";
@Component({
  selector: "app-article",
  templateUrl: "./article.component.html",
  styleUrls: ["./article.component.scss"],
})
export class ArticleComponent implements OnInit {
  @Input()
  article!: InterfaceBody;

  constructor() {
    console.log("este es el contructor");
    console.log("this.article");
  }

  ngOnInit(): void {}
}
