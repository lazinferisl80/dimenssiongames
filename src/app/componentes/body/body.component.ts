import { Component, OnInit } from "@angular/core";

import { InterfaceBody, Image } from "./Interface-body";

@Component({
  selector: "app-body",
  templateUrl: "./body.component.html",
  styleUrls: ["./body.component.scss"],
})
export class BodyComponent implements OnInit {
  articleWitcher: InterfaceBody | any = {};
  articleCiberpunk: InterfaceBody | any = {};
  imagewitcher: Image | any = {};
  imageCiberpunk: Image | any = {};

  constructor() {
    this.articleWitcher.productora = "CD PROYECT";
    this.articleWitcher.titulo = "The Witcher";
    this.articleWitcher.age = "24 / 10 / 2007";
    this.articleWitcher.categoria = "aventuras";
    this.articleWitcher.argumento = "un brujo matando bichos";
    this.imagewitcher.url =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2ywXsXZyqU2XsSHrLbGfEkuaoq4giRfosYQ&usqp=CAU";
    this.imagewitcher.alt = "imagen de the witcher";

    this.articleCiberpunk.productora = "CD PROYECT";
    this.articleCiberpunk.titulo = " CiberPunk 2077";
    this.articleCiberpunk.age = "4 / 12 / 2020";
    this.articleCiberpunk.categoria = "aventuras grafica";
    this.articleCiberpunk.argumento = "buscandose la vida";
    this.imageCiberpunk.url =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQA5QLXTfRWLe3elyAvoobrajoauamYKMqSqQ&usqp=CAU";
    this.imageCiberpunk.alt = "imagen ciberpunk";
  }

  ngOnInit(): void {
    this.articleWitcher.productora = "CD PROYECT";
    this.articleWitcher.titulo = "The Witcher";
    this.articleWitcher.age = "24 / 10 / 2007";
    this.articleWitcher.categoria = "aventuras";
    this.articleWitcher.argumento = "un brujo matando bichos";
    this.imagewitcher.url =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2ywXsXZyqU2XsSHrLbGfEkuaoq4giRfosYQ&usqp=CAU";
    this.imagewitcher.alt = "imagen de the witcher";

    this.articleCiberpunk.productora = "CD PROYECT";
    this.articleCiberpunk.titulo = " CiberPunk 2077";
    this.articleCiberpunk.age = "4 / 12 / 2020";
    this.articleCiberpunk.categoria = "aventuras grafica";
    this.articleCiberpunk.argumento = "buscandose la vida";
    this.imageCiberpunk.url =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQA5QLXTfRWLe3elyAvoobrajoauamYKMqSqQ&usqp=CAU";
    this.imageCiberpunk.alt = "imagen ciberpunk";
  }
}
