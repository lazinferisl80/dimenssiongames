import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Interface } from "./models/Interface";
import { ServiceGamesService } from "../../servios/service-games.service";

@Component({
  selector: "app-form-games",
  templateUrl: "./form-games.component.html",
  styleUrls: ["./form-games.component.scss"],
})
export class FormGamesComponent implements OnInit {
  @Input() post: any;
  public formGames: FormGroup | any = {};

  public submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private serviceGamesService: ServiceGamesService
  ) {
    this.formGames = this.formBuilder.group({
      name: ["", [Validators.required, Validators.minLength(4)]],
      nickname: ["", [Validators.required, Validators.minLength(4)]],
      age: ["", Validators.required],
      password: ["", [Validators.required, Validators.minLength(8)]],
      game: ["", [Validators.required]],
    });
  }

  ngOnInit(): void {
    /*Empty*/
  }

  public onSubmit(): void {
    this.submitted = true;

    if (this.formGames.valid) {
      const DimmessionGamesForm: Interface = {
        name: this.formGames.get("name").value,
        nickname: this.formGames.get("nickname").value,
        age: this.formGames.get("age").value,
        password: this.formGames.get("password").value,
        game: this.formGames.get("game").value,
      };
      this.serviceGamesService.postGames(DimmessionGamesForm);

      console.log(DimmessionGamesForm);
      this.formGames.reset();
      this.submitted = false;
    }
  }
}
