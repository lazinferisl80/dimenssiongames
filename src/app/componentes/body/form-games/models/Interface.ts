export interface Interface {
  name: string;
  nickname: string;
  age: number;
  password: string;
  game: string;
}
