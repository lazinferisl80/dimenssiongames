export interface InterfaceBody {
  productora: string;
  titulo: string;
  age: string;
  categoria: string;
  argumento: string;
  image: Image;
  alt: string;
}
export interface Image {
  url: string;
  alt: string;
}
