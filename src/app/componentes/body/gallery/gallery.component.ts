import { Component, OnInit } from "@angular/core";
import { ServiceGamesService } from "../../servios/service-games.service";
import { InterfaceBody } from "../Interface-body";

@Component({
  selector: "app-gallery",
  templateUrl: "./gallery.component.html",
  styleUrls: ["./gallery.component.scss"],
})
export class GalleryComponent implements OnInit {
  public myGames: InterfaceBody[] | null = null;
  constructor(private serviceGamesService: ServiceGamesService) {}

  ngOnInit(): void {}
}
