import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiosComponent } from './servios.component';

describe('ServiosComponent', () => {
  let component: ServiosComponent;
  let fixture: ComponentFixture<ServiosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
