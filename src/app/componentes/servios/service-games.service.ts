import { Injectable } from "@angular/core";
import { InterfaceBody } from "../body/Interface-body";

@Injectable({
  providedIn: "root",
})
export class ServiceGamesService {
  private games: InterfaceBody[] = [];

  constructor() {}

  getGames(): InterfaceBody[] {
    return this.games;
  }

  postGames(games: InterfaceBody): void {
    this.games.push(games);
  }
}
